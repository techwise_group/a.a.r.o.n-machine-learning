# A.A.R.O.N - Automatic License Plate Detection System

# Purpose -  Detects license plates and extracts the text from an image and real time video feed using Tensorflow object detection and Optical character recognition.

## Setup
This project was created in the Jupyter notebook environment. We decided to use Jupyter notebooks because it can illustrate the analysis process step by step by arranging the stuff like code, images, text, output, etc, making it popular in the machine learning industry. 

After cloning the repository...
1. Create a new virtual environment:
    python -m venv aaronsys
2. Activate your virtual environment: 
    .\aaronsys\Scripts\activate 
3. Install necessary dependencies and add virtual environment to the Python Kernel:
    python -m pip install --upgrade pip
    pip install ipykernel
    python -m ipykernel install --user --name=aaronsys

## Running A.A.R.O.N.
1. WARNING: SKIP BLOCKS 6-7 TO AVOID TRAINING THE MODEL ALL OVER AGAIN.
1. Start from the top of the notebook and execute the imports.
2. Run the block titled: "Detection from Real Time" to open your webcamera and test the program.

## How it works
A.A.R.O.N. is an automatic license plate detection system whose purpose is to detect license plates from live video feed and extract and store the license plate data and the detected images. The extracted license plates are then cross referenced against the current active amber alert license plates which will notify the user if a match is found. A.A.R.O.N. was integrated into an app so that civillians can use their mobile phone cameras to help law enforcement. 

First, we downloaded a pre-annotated license plate dataset from Kaggle and trained the Tensorflow Object Detection model with the data. We decided to use machine learning because unlike simple computer vision detection, a machine learning model will give us more accuracy as well as the ability to detect different types of number plates.

After training the model, we tested its detection abilities using OpenCV to open a webcam window. 
![Screenshot (367)](https://user-images.githubusercontent.com/98305390/216882853-22f07064-583f-49b1-801c-166a41e9718f.png)

Once we knew the detection was working, we applied Optical Character Recognition (OCR) to our model so that it would be able to read the license plates that it detects. After lots of testing and experimenting with error thresholds, we exported the model and sent it over to our app development team. From there, we worked on incorporating our model into a mobile environment 

## Resources used
https://www.tensorflow.org/hub/tutorials/object_detection
https://www.kaggle.com/datasets/andrewmvd/car-plate-detection
https://github.com/tensorflow/models
https://www.tensorflow.org/install/source_windows
https://builtin.com/data-science/python-ocr
https://github.com/JaidedAI/EasyOCR